#include "wrapper.hpp"

/* Include Default Begin */
#include <stdint.h>
#include <cmath>
/* Include Default End */


/* Include Private Begin */
#include "i2c.h"
#include "tim.h"
#include "gpio.h"
/* Include Private End */
#define ADDRESS 0x6B<<1//角速度、加速度のレジスタのI2Cのアドレス
#define GYRO_REGISTA_START 0x18//角速度のデータが入っているレジスタのアドレス
#define FRENQUENCY 1000.0//tim6の周波数
#define DEBUG_MODE
#define RC_FILTER_ENABLE 1
/* Class Constructor Begin */

/* Class Constructor End */


/* Variable Begin */
int16_t angularVelocity[3] = {0,0,0};
int16_t angularVelocityError[3] = {0,0,0};
float deg[3];
float rad[3];
float miniDeg[3];
float fixedAngularVelocity[3] = {0,0,0};
#ifdef DEBUG_MODE
uint32_t looping = 0;
#endif //DEBUG_MODE
/* Variable End */

void init(void){
	HAL_TIM_Base_Start_IT(&htim6);
	uint8_t gyroConfig[]={0b00101010,0b00000000,0b00001001};//CTRL_REG1_G,CTRL_REG2_G,CTRL_REG3_G
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x10,1,gyroConfig,sizeof(gyroConfig),100)){
	}
	uint8_t on = 0b00111000;
	//CTRL_REG4
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x1E,1,&on,1,100)){
	}

	uint8_t check = 0b00000100;
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&check,1,100)){
	}

}

void loop(void){
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13) == GPIO_PIN_SET){

	}else{
		for(uint8_t i=0;i<3;i++){
			deg[i] = 0;
		}
	}
	setConfig();
	getValue();
	for(uint8_t i=0;i<3;i++){
		setCriteria(i);
	}


}

/* Function Body Begin */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim == &htim6){
		calcValue();
	}

}
void getValue(){
	uint8_t data[6];
	static uint8_t beforeData[3];
	static uint16_t tickCount = 0;
	HAL_I2C_Mem_Read(&hi2c1,ADDRESS,GYRO_REGISTA_START,1,(uint8_t*)data,sizeof(data),0xF);
	for(uint8_t i = 0;i<3;i++){
		angularVelocity[i] = (int16_t)data[2*i+1]<<8 | data[2*i];
		if(i == 1){
			angularVelocity[i] *= -1;
		}
		angularVelocity[i] -= angularVelocityError[i];
		if(!(data[2*i] == beforeData[i])){
			beforeData[i] = data[2*i];
			tickCount = HAL_GetTick();
		}
		if(HAL_GetTick()-tickCount > 100){
			setConfig();
		}
	}

}
void setConfig(){
	uint8_t gyroConfig[]={0b00101010,0b00000000,0b00001001};//CTRL_REG1_G,CTRL_REG2_G,CTRL_REG3_G
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x10,1,gyroConfig,sizeof(gyroConfig),0xF);
	uint8_t on = 0b00111000;
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x1E,1,&on,1,0xF);
	uint8_t check = 0b00000100;
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&check,1,0xF);
	//uint8_t itExam = 0b10000010;
	//HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&itExam,1,100);
}
void setCriteria(uint8_t number){
	static int16_t beforeAngularVelocity[3] = {0,0,0};
	static uint32_t tickValue[3];
	static uint8_t fixCount[3] = {0,0,0};
	if(!(beforeAngularVelocity[number]-3 < angularVelocity[number] && angularVelocity[number] < beforeAngularVelocity[number]+3)){
		tickValue[number] = HAL_GetTick();
		beforeAngularVelocity[number] = angularVelocity[number];
	}

	if(HAL_GetTick() - tickValue[number] > 100){
		angularVelocityError[number] += beforeAngularVelocity[number];
	}


	if(HAL_GetTick() - tickValue[number] > 110 && fixCount[number] < 1){
		fixedAngularVelocity[number] = 0;
		deg[number] = 0;
		fixCount[number]++;
	}


}
void calcValue(){
	static float beforeFixedAngularVelocity[3] = {0,0,0};
	float coefficient[3];
	for(uint8_t i=0;i<3;i++){
		beforeFixedAngularVelocity[i] = fixedAngularVelocity[i];
		fixedAngularVelocity[i] = angularVelocity[i]/58.0;
#if RC_FILTER_ENABLE
		float k = 0.5;
		fixedAngularVelocity[i] = beforeFixedAngularVelocity[i]*(1-k) + fixedAngularVelocity[i]*k;
#endif
		miniDeg[i] = (fixedAngularVelocity[i]+beforeFixedAngularVelocity[i])/FRENQUENCY/2;

	}
	for(uint8_t v=0; v < 3;v++){
		switch(v){
			case 0:
				coefficient[0] = 1;
				coefficient[1] = sinf(rad[0])*tanf(rad[1]);
				coefficient[2] = cosf(rad[0])*tanf(rad[1]);
				break;
			case 1:
				coefficient[0] = 0;
				coefficient[1] = cosf(rad[0]);
				coefficient[2] = -sin(rad[0]);
				break;
			case 2:
				coefficient[0] = 0;
				coefficient[1] = sinf(rad[0])/cosf(rad[1]);
				coefficient[2] = cosf(rad[0])/cosf(rad[1]);

		}

		deg[v] += miniDeg[0]*coefficient[0] + miniDeg[1]*coefficient[1] + miniDeg[2]*coefficient[2];
		if(deg[v] > 180){
			deg[v] -= 360;
		}
		if(deg[v] < -180){
			deg[v] += 360;
		}
		rad[v] =  deg[v]*M_PI/180;
	}

}
/* Function Body End */
