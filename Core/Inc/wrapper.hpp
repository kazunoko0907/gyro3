#ifndef WRAPPER_HPP_
#define WRAPPER_HPP_
#include <stdint.h>
/* Enum Begin */

/* Enum End */


/* Struct Begin */

/* Struct End */

#ifdef __cplusplus
extern "C" {
#endif

void init(void);
void loop(void);

#ifdef __cplusplus
};
#endif
/* Function Prototype Begin */
void getValue();
void setCriteria(uint8_t);
void setConfig();
void calcValue();
void useThresholdFilta(uint8_t,float);
/* Function Prototype End */


#endif /* WRAPPER_HPP_ */
